%global debug_package %{nil}

Name:           python-tqdm
Version:        4.66.4
Release:        1
Summary:        A Fast and Extensible Progress Bar for Python and CLI
License:        MPL-2.0 and MIT
URL:            https://github.com/tqdm/tqdm
Source0:        https://files.pythonhosted.org/packages/source/t/tqdm/tqdm-%{version}.tar.gz

BuildRequires:  python3-devel python3-setuptools gcc python3-toml python3-setuptools_scm
BuildRequires:  python3-pip  python3-hatchling python3-hatch-vcs python3-wheel

%description
tqdm derives from the Arabic word taqaddum which can mean "progress". Instantly 
make your loops show a smart progress meter - just wrap any iterable with 
tqdm(interable), and you are done!

%package     -n python3-tqdm
Summary:        A Fast and Extensible Progress Bar for Python and CLI

%{?python_provide:%python_provide python3-tqdm}

%description -n python3-tqdm
tqdm derives from the Arabic word taqaddum which can mean "progress". Instantly 
make your loops show a smart progress meter - just wrap any iterable with 
tqdm(interable), and you are done!

%package_help

%prep
%autosetup -n tqdm-%{version} -p1

%build
%pyproject_build

%install
%pyproject_install

mkdir -p %{buildroot}%{_mandir}/man1/
mv -v %{buildroot}%{python3_sitelib}/tqdm/tqdm.1 %{buildroot}%{_mandir}/man1/

%files -n python3-tqdm
%defattr(-,root,root)
%doc README.rst examples
%license LICENCE
%{_bindir}/tqdm
%{python3_sitelib}/tqdm-*.dist-info/
%{python3_sitelib}/tqdm/

%files help
%defattr(-,root,root)
%{_mandir}/man1/tqdm.1*

%changelog
* Sat May 11 2024 dongjiao <dongjiao@kylinos.cn> - 4.66.4-1
- Upgrade package to version 4.66.4
  - rich: fix completion
  - minor framework updates & code tidy

* Mon May 06 2024 yaoxin <yao_xin001@hoperun.com> - 4.66.2-2
- Fix CVE-2024-34062

* Sat Feb 17 2024 xu_ping <707078654@qq.com> - 4.66.2-1
- Upgrade package to version 4.66.2

* Thu Apr 6 2023 liyanan <thistleslyn@163.com> - 4.65.0-1
- Upgrade package to version 4.65.0

* Fri Dec 09 2022 liukuo <liukuo@kylinos.cn> - 4.64.1-2
- License compliance rectification

* Wed Dec 07 2022 chendexi <chendexi@kylinos.cn> - 4.64.1-1
- Upgrade package to version 4.64.1

* Wed Aug 3 2022 kkz <zhaoshuang@uniontech.com> - 4.64.0-1
- Update to 4.64.0

* Thu Jun 24 2021 hanhui <hanhui15@huawei.com> - 4.56.0-2
- add BuildRequires:gcc python3-toml python3-setuptools_scm

* Mon Feb 1 2021 chengguipeng <chengguiopeng1@huawei.com> - 4.56.0-1
- Update to 4.56.0

* Thu May 28 2020 huanghaitao <huanghaitao8@huawei.com> - 4.28.1-2
- Remove tests

* Wed Mar 4 2020 hexiujun <hexiujun1@huawei.com> - 4.28.1-1
- Package init
